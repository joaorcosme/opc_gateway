// =================================================================
// UNIVERSIDADE FEDERAL DE MINAS GERAIS
// Sistemas Distribuidos para Automacao - Trabalho Final
//
// Felipe Carvalho
// Joao Victor Cosme
// Vinicius Venancio
// =================================================================


#include <winsock2.h>
#pragma comment (lib, "Ws2_32.lib")

#include "..\UtilLib\Network.h"
#include "..\UtilLib\Utils.h"
#pragma comment (lib, "UtilLib.lib")

#include <conio.h> 

#include <condition_variable>
#include <iostream>
#include <mutex>
#include <string>
#include <thread>


int main(int argc, char** argv) {

    std::condition_variable cv;
    bool writeMode = false;
    bool quit = false;

    WSADATA wsa;
    if (WSAStartup(MAKEWORD(2,2), &wsa) != 0) {
        return 1;
    }

    if (argc != 3) {
        utils::exitOnError("Argument error: [IPv4/6] [port]");
    }

    std::cout << "-------------------------"   << std::endl;
    std::cout << "     TCP/IP Client       "   << std::endl;
    std::cout << "-------------------------\n" << std::endl;

    int  peersock;
    network::setupTCPClient(argv[1], argv[2], peersock);

    // TODO: use std::mutex before using 'peersock' in each thread

    // Lambda task #1: check read/write mode and quit event
    auto readKeyboard = [&]() {
        std::mutex mtx;
        std::unique_lock<std::mutex> lock(mtx);

        switch ( _getch()) {
        case 32: // space character
            writeMode = true;
            cv.wait(lock);
            break;
        case 'q':
            quit = true;
        default:
            break;
        }
    };

    // Lambda task #2: allow user to write new values to the tags
    auto getMesageAndSend = [&]() {
        if (writeMode) { // TODO: change to condition_var and wait() to avoid high use of CPU
            Sleep(50); // ... allow last recv() to print

            std::cout << "\nA <tag port> <value>" << std::endl;
            std::cout << "D <tag port> <value>"   << std::endl;
            std::cout << "---------------------"  << std::endl;
            std::cout << "[Write Mode] >>  ";

            std::string toSend;
            std::getline(std::cin, toSend);

            network::safeSend(peersock, toSend, true /* echo */);
            writeMode = false;
            cv.notify_one();
        }
    };

    // Lambda task #3: receive new message if a tag value is updated
    auto recvMessageAndPrint = [&]() {      
        std::string msg;
        network::safeRecv(peersock, msg, !writeMode /* echo */);
    };

    // General routine to execute each of the threads
    auto exec = [&quit](std::function<void(void)> loop) {
        while (!quit) loop();
    };

    std::thread t1(exec, readKeyboard);
    std::thread(exec, getMesageAndSend).detach();
    std::thread(exec, recvMessageAndPrint).detach();
    t1.join();

    closesocket(peersock);
    WSACleanup();
    return 0;
}