#ifndef OPC_H
#define OPC_H

#include "opcda.h"

#include "..\UtilLib\Utils.h"
#pragma comment (lib, "UtilLib.lib")

#include <iomanip>
#include <sstream>
#include <string>



namespace opc {

class OpcItem {
  public: 
    OpcItem(wchar_t* id);
    bool hasChanged();
    std::string toString();

    wchar_t* itemID;
    std::string itemID_str;

    OPCITEMSTATE currentState;
    OPCITEMSTATE prevState;
    OPCHANDLE hServerItem{ 0 };
};



IOPCServer* InstantiateServer(wchar_t ServerName[]);
void AddTheGroup(IOPCServer* pIOPCServer, IOPCItemMgt* &pIOPCItemMgt, OPCHANDLE& hServerGroup);
void AddTheItem(IOPCItemMgt* pIOPCItemMgt, OpcItem& item);
void ReadItem(IUnknown* pGroupIUnknown, OPCHANDLE hServerItem, OPCITEMSTATE& varState);
void RemoveItem(IOPCItemMgt* pIOPCItemMgt, OPCHANDLE hServerItem);
void RemoveGroup(IOPCServer* pIOPCServer, OPCHANDLE hServerGroup);


} // namespace opc


#endif // OPC_H