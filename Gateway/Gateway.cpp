// =================================================================
// UNIVERSIDADE FEDERAL DE MINAS GERAIS
// Sistemas Distribuidos para Automacao - Trabalho Final
//
// Felipe Carvalho
// Joao Victor Cosme
// Vinicius Venancio
// =================================================================

#include <winsock2.h>
#pragma comment (lib, "Ws2_32.lib")

#include "..\UtilLib\Network.h"
#include "..\UtilLib\Utils.h"
#pragma comment (lib, "UtilLib.lib")

#include "opcda.h"
#include "opcerror.h"
#include "OPCInterface.h"

#include <atlbase.h> // required for using the "_T" macro
#include <conio.h>
#include <ObjIdl.h>

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <sstream>
#include <thread>
#include <vector>

#define TCP_SERVER_TAG "[TCP_SERVER]"
#define READ_TIMEOUT 1000


#define DEBUG_PRINTLN(msg) if(1) { std::cout << "DEBUG: " << msg << std::endl; }

//#define OPC_SERVER_NAME L"Matrikon.OPC.Simulation.1"
#define OPC_SERVER_NAME L"ECA.OPCDAServer215"


// The OPC DA Spec requires that some constants be registered in order to use
// them. The one below refers to the OPC DA 1.0 IDataObject interface.
UINT OPC_DATA_TIME = RegisterClipboardFormat (_T("OPCSTMFORMATDATATIME"));


std::atomic_flag consoleAccess;
std::mutex socketAccess;
std::string toSend;

void recvDataUpdates(const bool& quit, const int sock) {
    std::string msg;
    while (!quit) {
        socketAccess.lock();
        DEBUG_PRINTLN("New attempt to receive data");
        if (!quit) network::safeRecv(sock, msg, true /* echo */);

        // TODO: parse the message and write the corresponding OPC tag

        socketAccess.unlock();
        Sleep(50); // give a chance to the other thread
    }
}

void sendUpdatedData(const bool& quit, const int sock, std::condition_variable& cv) {

    std::mutex mtx;
    std::unique_lock<std::mutex> lock(mtx);

    while (!quit) {
        cv.wait(lock);
        DEBUG_PRINTLN("--Send thread-- notified about data change");
        socketAccess.lock();
        DEBUG_PRINTLN("--Send thread-- got acces to the socket");
        if (!quit) network::safeSend(sock, toSend);

        socketAccess.unlock();   
    }
}

void runTCPServer(char* port, const bool& quit, std::condition_variable& cv) {

    int serverSock;
    network::setupTCPServer(port, serverSock);

    while (consoleAccess.test_and_set()) {}
    std::cout << TCP_SERVER_TAG << " Waiting TCP connection..." << std::endl;

    int peerSock = network::acceptTCPConnection(serverSock);
    std::cout << TCP_SERVER_TAG << " New connection established\n" << std::endl;
    consoleAccess.clear();
    closesocket(serverSock);

    // t1 & t2 will compete for access to the socket
    std::thread t1(sendUpdatedData, std::ref(quit), peerSock, std::ref(cv));
    std::thread t2(recvDataUpdates, std::ref(quit), peerSock);
    t1.join();
    t2.join();
}

int main(int argc, char** argv) {

    WSADATA wsa;
    if (WSAStartup(MAKEWORD(2,2), &wsa) != 0) {
        return 1;
    }

    std::cout << "-------------------------"   << std::endl;
    std::cout << "   Gateway OPC-TCP/IP    "   << std::endl;
    std::cout << "-------------------------\n" << std::endl;

    bool quit = false;

    // Thread #1: wait for the quit event
    std::thread readKeyboardThread([&quit]() {
        while(!quit) quit = _getch() == 'q'; 
    });

    readKeyboardThread.detach();

    // -----------------------------------------
    // Starting OPC Client # BEGIN
    
    std::vector<opc::OpcItem> opcItens {
        opc::OpcItem(L"Escrita.Analog1"),
        opc::OpcItem(L"Escrita.Analog2"),
        opc::OpcItem(L"Escrita.Digit1"),
        opc::OpcItem(L"Escrita.Digit2"),
        opc::OpcItem(L"Escrita.Digit3"),
        opc::OpcItem(L"Escrita.Digit4"),
        opc::OpcItem(L"Escrita.Digit5"),
        opc::OpcItem(L"Escrita.Digit6"),
        opc::OpcItem(L"Escrita.Digit7"),
        opc::OpcItem(L"Escrita.Digit8"),
        opc::OpcItem(L"Leitura.Analog1"),
        opc::OpcItem(L"Leitura.Analog2"),
        opc::OpcItem(L"Leitura.Digit1"),
        opc::OpcItem(L"Leitura.Digit2"),
        opc::OpcItem(L"Leitura.Digit3"),
        opc::OpcItem(L"Leitura.Digit4"),
        opc::OpcItem(L"Leitura.Digit5")
    };

    while (consoleAccess.test_and_set()) {}

    std::cout << "# Initializing the COM environment..." << std::endl;
    CoInitialize(NULL);

    std::cout << "# Intantiating OPC Server..." << std::endl;
    IOPCServer* pIOPCServer = opc::InstantiateServer(OPC_SERVER_NAME);
    
    // Add the OPC group the OPC server and get an handle to the IOPCItemMgt interface
    std::cout << "# Adding a group in the INACTIVE state for the moment...\n\n" << std::endl;
    IOPCItemMgt* pIOPCItemMgt = nullptr;
    OPCHANDLE hServerGroup;
    opc::AddTheGroup(pIOPCServer, pIOPCItemMgt, hServerGroup);


    // Add all itens
    for (auto& item : opcItens) {
        // Convert from wchar_t* to char* in order to print the item name in the console
        std::cout << "-- Adding item \"" << item.itemID_str << "\" to the group" << std::endl;
        opc::AddTheItem(pIOPCItemMgt, item);
    }

    std::cout << "\n" << std::endl;
    consoleAccess.clear();

    // Starting OPC Client # END
    // -----------------------------------------

    std::condition_variable cv;
    char port[8] = "1234";
    std::thread tcpServer(runTCPServer, port, std::ref(quit), std::ref(cv));
    tcpServer.detach();

    while (!quit) {

        //Synchronous read of the OPC itens

        bool hasChanged = false;
        for (auto& item : opcItens) {
            opc::ReadItem(pIOPCItemMgt, item.hServerItem, item.currentState);
            if (!hasChanged) {
                hasChanged = item.hasChanged();
            }
            item.prevState = item.currentState;
        }

        if (hasChanged) { // send data through TCP socket...
            DEBUG_PRINTLN("--Main-- Has changed");
            std::stringstream msgBuilder;
            for (auto& item : opcItens) {
                msgBuilder << item.toString() << "\n";
            }
            toSend = msgBuilder.str();
            cv.notify_one();
            DEBUG_PRINTLN("--Main-- Notified data change");
        }

        hasChanged = false;
        Sleep(1000);
    }

    cv.notify_one(); // unblock waiting thread...

    // -----------------------------------------
    // Shutting down OPC Client # BEGIN
    

    // Remove all OPC itens
    std::cout << "\n# Removing all OPC itens...\n";
    for (auto& item : opcItens) {
        opc::RemoveItem(pIOPCItemMgt, item.hServerItem);
    }

    // Remove the OPC group
    std::cout << "# Removing the OPC group object..." << std::endl;
    pIOPCItemMgt->Release();
    opc::RemoveGroup(pIOPCServer, hServerGroup);

    // Release the interface references
    std::cout << "# Removing the OPC server object..." << std::endl;
    pIOPCServer->Release();

    // Close the COM library
    std::cout << "# Releasing the COM environment..."  << std::endl;
    CoUninitialize();


    // Shutting down OPC Client # END
    // -----------------------------------------

    WSACleanup();
    std::cout << "\n Hit any key to quit..."  << std::endl;
    _getch();

    return 0;
}