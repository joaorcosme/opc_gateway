#include "Utils.h"

#include <iostream>
#include <string>
#include <sstream>
#include <vector>

#include <stdlib.h>

void
utils::exitOnError(const std::string error) {
    std::string message("[ERROR]:  " + error);
    perror(message.c_str());
    exit(1);
}

void
utils::removeLineBreak(std::string& str) {
    if(!str.empty() && (str[str.length() - 1] == '\r')) { // CR character
        str.erase(str.length() - 1, 1);
    }
    if(!str.empty() && (str[str.length() - 1] == '\n')) { // LF character
        str.erase(str.length() - 1, 1);
    }
}

void
utils::getMessageTokens(const std::string message,
    std::vector<std::string>& tokens) {
    using namespace std;
    istringstream toParse(message);
    string token;

    while(getline(toParse, token, ' ')) {
        removeLineBreak(token);
        tokens.push_back(token);
    }
}

std::string
utils::fileTimeToString(const FILETIME& ft) {
    SYSTEMTIME st;
    char szLocalDate[255], szLocalTime[255];

    FileTimeToSystemTime(&ft, &st);
    GetDateFormat(LOCALE_USER_DEFAULT, DATE_SHORTDATE, &st, NULL, szLocalDate, 255);
    GetTimeFormat(LOCALE_USER_DEFAULT, 0, &st, NULL, szLocalTime, 255);
    
    std::stringstream ss;
    ss << szLocalDate << " " << szLocalTime;
    return ss.str();
}