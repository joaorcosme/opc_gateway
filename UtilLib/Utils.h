#ifndef UTILS_H
#define UTILS_H


#include <windows.h>

#include <string>
#include <vector>

namespace utils {

// print error message and terminate process
void exitOnError(const std::string error);

// remove CR, LF characters at the end
void removeLineBreak(std::string& str);

// tokenize message, inserting each token in the vector
void getMessageTokens(const std::string message,
    std::vector<std::string>& tokens);

std::string fileTimeToString(const FILETIME& ft);

} // namespace utils

#endif