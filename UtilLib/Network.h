#ifndef NETWORK_H
#define NETWORK_H

#include <string>

namespace network {

// send message through sock, check for error
// and print to stdout if echo is true
void safeSend(int sock, std::string& msg, bool echo = false);


// receive message through sock, check for error
// and print to stdout if echo is true
void safeRecv(int sock, std::string& msg, bool echo = false);


// connect client to server at addr:port
// a socket is created and stored in peersock
void setupTCPClient(char* addr, char* port, int& peersock);


// configure server to listen to clients through "port"
void setupTCPServer(char* port, int& serversock);


// wait for client conection on a configured socket
int acceptTCPConnection(const int serversock);

} // namespace network

#endif