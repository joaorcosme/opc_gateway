#include <winsock2.h>
#include <ws2tcpip.h>

#pragma comment (lib, "Ws2_32.lib")

#include <cassert>
#include <iostream>
#include <sstream>
#include <string>
#include <string.h>

#include "Network.h"
#include "Utils.h"

void
network::safeSend(int sock, std::string& msg, bool echo) {
    const char* toSend = msg.c_str();
    if(send(sock, toSend, strlen(toSend), 0) < 0) {
        utils::exitOnError("send()");
    }
    utils::removeLineBreak(msg); // there may be a \n or \r\n at the end
    if(echo) {
        std::cout << "Sent:     " << msg << std::endl;
    }
}

void
network::safeRecv(int sock, std::string& msg, bool echo) {
    const unsigned BUFSIZE = 2048;
    char buf[BUFSIZE];
    int len = recv(sock, buf, BUFSIZE, 0);
    if(len < 0) {
        //utils::exitOnError("recv()");
        return;
    }

    assert(len < BUFSIZE);

    buf[len] = '\0'; // set end of string
    msg = buf;
    utils::removeLineBreak(msg); // there may be a \n or \r\n at the end
    if(echo) {
        std::cout << msg << "\n" << std::endl;
    }
}


void
network::setupTCPClient(char* addr, char* port, int& peersock) {

    struct addrinfo hints;
    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    struct addrinfo* result = NULL;
    struct addrinfo* copyResult;

    // set up struct "result"
    if (getaddrinfo(addr /*node*/, port /*service*/, &hints, &result) != 0) {
        utils::exitOnError("getaddrinfo()");
    }

    copyResult = result;
    while (result != NULL) {

        // create socket
        peersock = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
        if (peersock == -1) {
            perror("Error:  socket()\n");
            result = result->ai_next; // there may be another valid address in res
            continue;
        }

        DWORD timeout = 500;
        int optlen = sizeof(DWORD);
        
        // recv will block for 'timeout' ms, at most
        setsockopt(peersock, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, optlen);

        // connect to server
        if (connect(peersock, result->ai_addr, result->ai_addrlen) == -1) {
            closesocket(peersock);
            perror("Error: connect()\n");
            result = result->ai_next;
            continue;
        }

        break;
    }

    if (result == NULL) {
        utils::exitOnError("Unable to establish connection");
    }

    freeaddrinfo(copyResult);
}


void
network::setupTCPServer(char* port, int& serversock) {
    const int LISTEN_BACKLOG = 128;

    struct addrinfo hints;
    ZeroMemory(&hints, sizeof(hints));
    hints.ai_flags = AI_PASSIVE;      // allow bind()
    hints.ai_family = AF_INET6;       // IPv6
    hints.ai_socktype = SOCK_STREAM;  // TCP


    struct addrinfo* result = NULL;
    struct addrinfo* copyResult;

    if (getaddrinfo(NULL /*node*/, port /*service*/, &hints, &result) != 0) {
        utils::exitOnError("getaddrinfo()");
    }

    copyResult = result;
    while(result != NULL) {
        // create socket
        serversock = socket(result->ai_family, result->ai_socktype, result->ai_protocol);

        // if socket is invalid...
        if (serversock == -1) {
            perror("[ERROR] socket()\n");
            result = result->ai_next; // ...try another valid address
        } else {

            // make sure IPv6_V6ONLY option is disabled
            int on = 0;
            setsockopt(serversock, IPPROTO_IPV6, IPV6_V6ONLY, (char *)&on, sizeof(on));
            
            DWORD timeout = 500;
            int optlen = sizeof(DWORD);

            // recv will block for 'timeout' ms, at most
            setsockopt(serversock, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, optlen);

            // bind port to socket
            if(bind(serversock, result->ai_addr, result->ai_addrlen) == -1) {
                closesocket(serversock);
                utils::exitOnError("bind()");
            }

            // configure to be a server that waits for clients
            if(listen(serversock, LISTEN_BACKLOG) < 0) {
                closesocket(serversock);
                utils::exitOnError("listen()");
            }

            break; // success
        }
    }

    if (result == NULL) {
        utils::exitOnError("Unable to establish connection");
    } else {
        // server is ready
        freeaddrinfo(copyResult);
    }
}


int
network::acceptTCPConnection(const int serversock) {
    // blocking call... wait connections from client
    int peersock = accept(serversock, NULL, NULL);
    if (peersock < 0) {
        closesocket(serversock);
        utils::exitOnError("accept()");
    }
    return peersock;
}